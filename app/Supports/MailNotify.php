<?php

namespace App\Supports;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailNotify extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $ccTo;
    public $bccTo;
    /**
     * Create a new data instance.
     *
     * @return void
     */

    public function __construct(array $data, array $ccTo, array $bccTo)
    {
        $this->data = $data;
        $this->ccTo = $ccTo;
        $this->bccTo = $bccTo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): MailNotify
    {
        return $this->from('tuantv98.it@gmail.com')
           ->to('tuantv@codluck.com')
            ->view('emails.sendMail', $this->data)
            ->subject('test-send-email');
    }

    /**
     * Get view send mail
     *
     * @return array
     */
    protected function getView(): array
    {
        return [];
    }
}
