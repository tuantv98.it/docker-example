<?php

namespace App\Supports;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ResponseResource
{
    /**
     * Response success
     *
     * @param array|null $data
     * @param string $message
     * @param int $httpCode
     * @param array $headers
     * @param int $options
     * @return JsonResponse
     */
    public function success(array $data = null, string $message = "success", int $httpCode = Response::HTTP_OK, array $headers = [], int $options = 0): JsonResponse
    {
        return $this->respond(1, $message, $data, $httpCode, $headers, $options);
    }

    /**
     * Response fail
     *
     * @param string $message
     * @param int $httpCode
     * @param array $headers
     * @param int $options
     * @return JsonResponse
     */
    public function fail(string $message = "fail", int $httpCode = Response::HTTP_EXPECTATION_FAILED, array $headers = [], int $options = 0): JsonResponse
    {
        return $this->respond(0, $message, [], $httpCode, $headers, $options);
    }


    /**
     * Response resource return
     *
     * @param int $code
     * @param string $message
     * @param array|null $data
     * @param int $httpCode
     * @param array $headers
     * @param int $options
     * @return JsonResponse
     */
    public function respond(int $code = 0, string $message = '', array $data = null, int $httpCode = Response::HTTP_OK, array $headers = [], int $options = 0): JsonResponse
    {
        $response = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ];

        return response()->json($response, $httpCode, $headers, $options);
    }
}
