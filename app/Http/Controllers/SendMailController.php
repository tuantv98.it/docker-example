<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use Illuminate\Http\JsonResponse;

class SendMailController extends Controller
{
    public function sendMail(): JsonResponse
    {
        $emails = ['tuantv@codluck.com'];
        $cc = ['tuantv@codluck.com'];
        $content = ['tuantv@codluck.com'];
        $bcc = ['tuantv@codluck.com'];
        $emailJob = new SendEmail($emails,$content,$cc,$bcc);
        dispatch($emailJob);
        return $this->success([]);
    }
}
