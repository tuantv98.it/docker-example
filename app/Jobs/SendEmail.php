<?php

namespace App\Jobs;

use App\Supports\MailNotify;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $contents;
    protected $mails;
    protected $cc;
    protected $bcc;

    /**
     * Create a new job instance.
     *
     * @param array $mails
     * @param array $contents
     * @param array $cc
     * @param array $bcc
     */
    public function __construct(array $mails, array $contents, array $cc, array $bcc)
    {
        $this->contents = $contents;
        $this->mails = $mails;
        $this->cc = $cc;
        $this->bcc = $bcc;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mails = array_unique($this->mails);
        $cc = array_unique($this->cc);
        $bcc = array_unique($this->bcc);
        Mail::to($mails)->send(new MailNotify($this->contents, $cc, $bcc));
    }
}
