<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\RepositoryInterface;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Repository\Eloquent\BaseRepository as L5Repository;

/**
 * Class BaseRepository
 * @property Model|Builder $model
 * @package App\Repositories
 */
abstract class BaseRepository extends L5Repository implements RepositoryInterface
{
    /**
     * @param array $conditions
     * @param array $columns
     * @return mixed
     * @throws RepositoryException
     */
    public function findWhereForUpdate(array $conditions, $columns = ['*'])
    {
        $this->applyConditions($conditions);

        $results = $this->model->lockForUpdate()->first($columns);

        $this->resetModel();

        return $this->parserResult($results);
    }

    public function findWhereFirst(array $conditions, $columns = ['*'])
    {
        $this->applyConditions($conditions);

        return $this->first($columns);
    }

    /**
     * @param $records
     * @param array $exclude
     * @return bool
     */
    public function insertOrUpdateBatch($records, array $exclude = [])
    {
        $columnsString = $valuesString = $updateString = '';
        $params = [];
        $size   = count($records);

        for ($i = 0; $i < $size; $i++) {
            $row = (array) $records[$i];
            if ($i == 0) {
                foreach ($row as $key => $value) {
                    $columnsString .= "$key,";
                    $updateString .= "$key=VALUES($key),";
                }
                $columnsString = rtrim($columnsString, ',');
                $updateString = rtrim($updateString, ',');
            } else {
                $valuesString .= ',';
            }

            $valuesString .= '(';

            foreach ($row as $key => $value) {
                $valuesString .= '?,';
                // CHANGE 20220510 条件文の変更
                //if (empty($value) && !is_float($value) && !is_int($value)) {
                if (empty($value) && $value !== "0" && $value !== 0 && !is_float($value) && !is_int($value)) {
                    $value = "";
                }
                array_push($params, $value);
            }

            $valuesString = rtrim($valuesString, ',');
            $valuesString .= ')';
        }

        $query = "INSERT INTO {$this->model->getTable()} ({$columnsString}) VALUES $valuesString ON duplicate KEY UPDATE $updateString";

        return $this->model->getConnection()->statement($query, $params);
    }

    /**
     * @param array $where
     */
    protected function applyConditions(array $where)
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                if (strtoupper($condition) == 'IN') {
                    $this->model = $this->model->whereIn($field, $val);
                } else if (strtoupper($condition) == 'NOT_IN') {
                    $this->model = $this->model->whereNotIn($field, $val);
                } else if (strtoupper($condition) == 'WHERE_BETWEEN') {
                    $this->model = $this->model->whereBetween($field, $val);
                } else if (strtoupper($condition) == 'LIKE') {
                    $this->model = $this->model->where($field, 'like', "%" . $val . "%");
                } else {
                      $this->model = $this->model->where($field, $condition, $val);
                }
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }
}
